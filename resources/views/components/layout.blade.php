<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link rel="stylesheet" href="https://kit.fontawesome.com/5148478db2.css" crossorigin="anonymous"> --}}
    <script src="https://kit.fontawesome.com/5148478db2.js" crossorigin="anonymous"></script>
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>New Site</title>
  
    
</head>
<body>
    
    <x-navbar />
<div class="container vh-min-100">
    {{$slot}}
</div>

<x-footer />


</body>
</html>

